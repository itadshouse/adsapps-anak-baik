package com.medialablk.instadraw;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.Toast;
import com.etsy.android.grid.StaggeredGridView;
//import com.startapp.android.publish.adsCommon.StartAppAd;
//import com.startapp.android.publish.adsCommon.StartAppSDK;
import java.io.File;

public class GalleryActivity extends AppCompatActivity {

	// Declare variables
	private String[] FilePathStrings;
	private String[] FileNameStrings;
	private File[] listFile;
	int length;
	StaggeredGridView grid;
	GridViewAdapter adapter;
	File file;
	ImageView ic_back, ic_slide;
	String appname;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		String appID = getResources().getString(R.string.appid);
		//StartAppSDK.init(this, appID, true);
		//StartAppAd.disableSplash();
		setContentView(R.layout.gridview_main);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		/*
		StartAppAd.showAd(this);
		*/
		if(getSupportActionBar() != null){
			getSupportActionBar().setDisplayShowTitleEnabled(false);
		}

		appname = getResources().getString(R.string.app_name);

		ic_back = (ImageView) toolbar.findViewById(R.id.ic_back);
		ic_back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onBackPressed();
			}
		});

		ic_slide = (ImageView) toolbar.findViewById(R.id.ic_slide);
		ic_slide.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent i = new Intent(GalleryActivity.this, SlideActivity.class);
				// Pass String arrays FilePathStrings
				i.putExtra("filepath", FilePathStrings);
				// Pass String arrays FileNameStrings
				i.putExtra("filename", FileNameStrings);
				// Pass click position
				i.putExtra("length", length);
				startActivity(i);
				finish();
			}
		});

		// Check for SD Card
		if (!Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			Toast.makeText(this, R.string.noSDCard, Toast.LENGTH_LONG)
					.show();
		} else {
			// Locate the image folder in your SD Card
			file = new File(Environment.getExternalStorageDirectory()
					+ File.separator + appname);
			// Create a new folder if no folder named Instadraw exist
			file.mkdirs();
		}

		if (file.isDirectory()) {
			listFile = file.listFiles();
			length = listFile.length;
			// Create a String array for FilePathStrings
			FilePathStrings = new String[listFile.length];
			// Create a String array for FileNameStrings
			FileNameStrings = new String[listFile.length];

			for (int i = 0; i < listFile.length; i++) {
				// Get the path of the image file
				FilePathStrings[i] = listFile[i].getAbsolutePath();
				// Get the name image file
				FileNameStrings[i] = listFile[i].getName();
			}
		}

		// Locate the GridView in gridview_main.xml
		grid = (StaggeredGridView) findViewById(R.id.gridview);
		View empty = findViewById(R.id.empty);


		// Pass String arrays to LazyAdapter Class
		adapter = new GridViewAdapter(this, FilePathStrings, FileNameStrings);
		// Set the LazyAdapter to the GridView
		if(adapter.isEmpty()){
			grid.setEmptyView(empty);
		}else{
			grid.setAdapter(adapter);
		}

		// Capture gridview item click
		grid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				Intent i = new Intent(GalleryActivity.this, ViewImage.class);
				// Pass String arrays FilePathStrings
				i.putExtra("filepath", FilePathStrings);
				// Pass String arrays FileNameStrings
				i.putExtra("filename", FileNameStrings);
				// Pass click position
				i.putExtra("position", position);
				startActivity(i);
				finish();
			}
		});
	}
}
