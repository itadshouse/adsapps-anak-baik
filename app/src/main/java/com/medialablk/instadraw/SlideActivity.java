package com.medialablk.instadraw;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;


public class SlideActivity extends Activity {
    // Declare Variable
    TextView text;
    ImageView imageview;
    String filePath;
    int length, a;
    String[] filepath;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        // Get the view from view_image.xml
        setContentView(R.layout.activity_slide);


        // Retrieve data from MainActivity on GridView item click
        Intent i = getIntent();

        // Get the position
        length = i.getExtras().getInt("length");

        // Get String arrays FilePathStrings
        filepath = i.getStringArrayExtra("filepath");

        // Get String arrays FileNameStrings
        String[] filename = i.getStringArrayExtra("filename");

        // Locate the ImageView in view_image.xml
        imageview = (ImageView) findViewById(R.id.image);


        AnimationDrawable animation = new AnimationDrawable();
        for (a = 0; a < length; a++) {
            Bitmap bmp = BitmapFactory.decodeFile(filepath[a]);
            Drawable d = new BitmapDrawable(getResources(), bmp);
            animation.addFrame(d, 4000);
        }
        animation.setEnterFadeDuration(1000);
        animation.setExitFadeDuration(1000);
        animation.setOneShot(false);

        ImageView imageAnim =  (ImageView) findViewById(R.id.image);
        imageAnim.setBackgroundDrawable(animation);
        // start the animation!
        animation.start();

    }

    public void onBackPressed(){
        Intent i = new Intent(SlideActivity.this, GalleryActivity.class);
        startActivity(i);
        finish();
    }
}