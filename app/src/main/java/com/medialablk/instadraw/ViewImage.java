package com.medialablk.instadraw;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;



//import com.startapp.android.publish.adsCommon.StartAppAd;
//import com.startapp.android.publish.adsCommon.StartAppSDK;

import java.io.File;

public class ViewImage extends Activity {
	// Declare Variable
	ImageView imageview;
	String filePath, appname;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//String appID = getResources().getString(R.string.appid);
		//StartAppSDK.init(this, appID, true);
		//StartAppAd.disableSplash();
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		// Get the view from view_image.xml
		setContentView(R.layout.view_image);

		appname = getResources().getString(R.string.app_name);

		// Retrieve data from MainActivity on GridView item click
		Intent i = getIntent();

		// Get the position
		int position = i.getExtras().getInt("position");

		// Get String arrays FilePathStrings
		String[] filepath = i.getStringArrayExtra("filepath");

		// Get String arrays FileNameStrings
		String[] filename = i.getStringArrayExtra("filename");

		// Locate the ImageView in view_image.xml
		imageview = (ImageView) findViewById(R.id.full_image_view);

		// Decode the filepath with BitmapFactory followed by the position
		Bitmap bmp = BitmapFactory.decodeFile(filepath[position]);

		// Set the decoded bitmap into ImageView
		imageview.setImageBitmap(bmp);

		filePath = filename[position];

	}

	public void deletephoto(View v){
		onDeletePressed();
	}

	public void sharephoto(View v){
		share();
	}

	public void share(){
		Intent intent = null;
		intent = new Intent(Intent.ACTION_SEND);
		intent.setType("image/*");
		File sdcard = new File(Environment.getExternalStorageDirectory()
				+ File.separator + appname);
		File file = new File(sdcard,filePath);
		Uri bmpUri = Uri.parse(file.getPath());
		intent.putExtra(Intent.EXTRA_STREAM, bmpUri);
		startActivity(Intent.createChooser(intent, getResources().getString(R.string.shareDrawing)));
	}

	public void delete(){
		File sdcard = new File(Environment.getExternalStorageDirectory()
				+ File.separator + appname);
		File file = new File(sdcard,filePath);
		if (file.exists()) {
			if (file.delete()) {
				Toast.makeText(ViewImage.this, R.string.imageDeleted,
						Toast.LENGTH_SHORT).show();
				onBackPressed();
			} else {
				Toast.makeText(ViewImage.this, R.string.imageNotDeleted,
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void onBackPressed(){
		Intent i = new Intent(ViewImage.this, GalleryActivity.class);
		startActivity(i);
		finish();
	}

	public void onDeletePressed(){
		final AlertDialog.Builder popDialog = new AlertDialog.Builder(this);
		popDialog.setTitle(R.string.delete);
		popDialog.setMessage(R.string.deleteMessage);

		popDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				delete();
			}
		});
		popDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {

			}
		});
		popDialog.show();
	}
}