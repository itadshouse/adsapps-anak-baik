package com.medialablk.instadraw;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.Toast;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
//import com.startapp.android.publish.adsCommon.StartAppAd;
//import com.startapp.android.publish.adsCommon.StartAppSDK;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import devlight.io.library.ntb.NavigationTabBar;


public class MainActivity extends AppCompatActivity implements EasyPermission.OnPermissionResult {
    CanvasView canvas = null;
    NavigationTabBar ntbOne, ntbTwo;
    ImageView ic_new, ic_gallery;
    Boolean mark = true;
    String appname;
    EasyPermission easyPermission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //StartApp Initialization
        //String appID = getResources().getString(R.string.appid);
        //StartAppSDK.init(this, appID, true);
        //StartAppAd.disableSplash();

        setContentView(R.layout.activity_main);

        //Runtime Permissions for Android 6 and above
        easyPermission = new EasyPermission();
        easyPermission.requestPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        //Top Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        appname = getResources().getString(R.string.app_name);
        ic_new = (ImageView) toolbar.findViewById(R.id.ic_new);
        ic_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });

        ic_gallery = (ImageView) toolbar.findViewById(R.id.ic_gallery);
        ic_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, GalleryActivity.class);
                startActivity(i);
            }
        });

        canvas = (CanvasView)this.findViewById(R.id.canvas);

        //Navigation Toolbar (Bottom)
        ntbOne = (NavigationTabBar) findViewById(R.id.ntbOne);
        dialogBgColors();
        final ArrayList<NavigationTabBar.Model> models5 = new ArrayList<>();
        models5.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_pencil), Color.rgb(173, 173, 173)
                ).build()
        );
        models5.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_eraser), Color.rgb(173 , 173, 173)
                ).build()
        );
        models5.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_size), Color.rgb(173 , 173, 173)
                ).build()
        );
        models5.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_pallete), Color.rgb(173 , 173, 173)
                ).build()
        );
        models5.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_save), Color.rgb(173 , 173, 173)
                ).build()
        );
        models5.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_more), Color.rgb(173, 173, 173)
                ).build()
        );
        ntbOne.setModels(models5);
        ntbOne.setModelIndex(0, true);
        ntbOne.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
            @Override
            public void onStartTabSelected(final NavigationTabBar.Model model, final int index) {

            }

            @Override
            public void onEndTabSelected(final NavigationTabBar.Model model, final int index) {
                if(index == 0){
                    canvas.setMode(CanvasView.Mode.DRAW);
                    mark = true;
                }else if(index == 1){
                    canvas.setMode(CanvasView.Mode.ERASER);
                    mark = false;
                }else if(index ==2) {
                    dialogSize();
                }else if(index == 3){
                    dialogColors();
                }else if(index == 4){
                    saveImage(canvas.getBitmap());
                }else if(index == 5){
                    ntbOne.setVisibility(View.GONE);
                    ntbTwo.setVisibility(View.VISIBLE);
                }
            }
        });

        ntbTwo = (NavigationTabBar) findViewById(R.id.ntbTwo);
        final ArrayList<NavigationTabBar.Model> models6 = new ArrayList<>();
        models6.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_share), Color.rgb(173, 173, 173)
                ).build()
        );
        models6.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_undo), Color.rgb(173 , 173, 173)
                ).build()
        );
        models6.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_redo), Color.rgb(173 , 173, 173)
                ).build()
        );
        models6.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_info), Color.rgb(173 , 173, 173)
                ).build()
        );
        models6.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_close), Color.rgb(173, 173, 173)
                ).build()
        );
        ntbTwo.setModels(models6);
        ntbTwo.setModelIndex(4, true);
        ntbTwo.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
            @Override
            public void onStartTabSelected(final NavigationTabBar.Model model, final int index) {

            }

            @Override
            public void onEndTabSelected(final NavigationTabBar.Model model, final int index) {
                if(index == 0){
                    Bitmap bmp = canvas.getBitmap();
                    String path = MediaStore.Images.Media.insertImage(getApplicationContext().getContentResolver(), bmp, appname, null);
                    Uri imageUri = Uri.parse(path);
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.putExtra(Intent.EXTRA_STREAM, imageUri);
                    share.setType("image/*");
                    share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(Intent.createChooser(share, getResources().getString(R.string.shareDrawing)));
                }else if(index == 1){
                    canvas.undo();
                }else if(index ==2) {
                    canvas.redo();
                }else if(index == 3){
                    Intent i = new Intent(MainActivity.this, AboutActivity.class);
                    startActivity(i);
                }else if(index == 4){
                    ntbOne.setVisibility(View.VISIBLE);
                    ntbTwo.setVisibility(View.GONE);
                }
            }
        });
    }

    //DialogBox to set Pencil stroke's width and Size of Eraser
    public void dialogSize(){
        final AlertDialog.Builder popDialog = new AlertDialog.Builder(this);
        final SeekBar seek = new SeekBar(this);
        seek.setPadding(40, 20, 40, 0);
        seek.setMax(75);
        seek.setKeyProgressIncrement(1);
        seek.setProgress((int) canvas.getPaintStrokeWidth());
        popDialog.setIcon(getResources().getDrawable(R.drawable.ic_size));
        popDialog.setTitle(R.string.pSize);
        popDialog.setView(seek);
        popDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                float f = (float) seek.getProgress();
                canvas.setPaintStrokeWidth(f);
                if (mark) {
                    ntbOne.setModelIndex(0, true);
                } else {
                    ntbOne.setModelIndex(1, true);
                }
            }
        });
        popDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        popDialog.show();
    }

    //Color picker DialogBox for Pencil
    public void dialogColors(){
        ColorPickerDialogBuilder
                .with(MainActivity.this)
                .setTitle(R.string.chooseColor)
                .initialColor(Color.RED)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .showAlphaSlider(false)
                .setOnColorSelectedListener(new OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int selectedColor) {

                    }
                })
                .setPositiveButton(R.string.ok, new ColorPickerClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        canvas.setPaintStrokeColor(selectedColor);
                        ntbOne.setModelIndex(0, true);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ntbOne.setModelIndex(0, true);
                    }
                })
                .build()
                .show();
    }

    //Color picker DialogBox for Background
    public void dialogBgColors(){
        ColorPickerDialogBuilder
                .with(MainActivity.this)
                .setTitle(R.string.chooseBgColor)
                .initialColor(canvas.getBaseColor())
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .showAlphaSlider(false)
                .setOnColorSelectedListener(new OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int selectedColor) {

                    }
                })
                .setPositiveButton(R.string.ok, new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        canvas.setBaseColor(selectedColor);
                        canvas.setBackgroundColor(selectedColor);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .build()
                .show();
    }

    public void onBackPressed(){
        final AlertDialog.Builder popDialog = new AlertDialog.Builder(this);
        popDialog.setTitle(R.string.exit);
        popDialog.setMessage(R.string.exitMessage);

        popDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                homeIntent.addCategory(Intent.CATEGORY_HOME);
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                finish();
            }
        });
        popDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        popDialog.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN )){
            Float sound = canvas.getPaintStrokeWidth();
            canvas.setPaintStrokeWidth(sound - 1);
        }else if ((keyCode == KeyEvent.KEYCODE_VOLUME_UP)){
            Float sound = canvas.getPaintStrokeWidth();
            canvas.setPaintStrokeWidth(sound + 1);
        }else if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
        }

        return true;
    }


    public void saveImage(Bitmap bmp){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String cdt = sdf.format(new Date());
        String root = Environment.getExternalStorageDirectory()
                .toString();
        File myDir = new File(root + "/"+appname);
        myDir.mkdirs();
        Random r = new Random();
        int num = r.nextInt(9999999 - 1000000 + 1) + 1000000;
        String fname = num + "_" + cdt + ".jpg";
        File file = new File(myDir, fname);

        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            Toast.makeText(getApplicationContext(), R.string.photoSaved,
                    Toast.LENGTH_LONG).show();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), R.string.photoNotSaved,
                    Toast.LENGTH_LONG).show();
        }
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        easyPermission.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onPermissionResult(String permission, boolean isGranted) {
        switch (permission) {
            case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                if (isGranted) {
                    Log.e("writeStorage", "granted");
                    //Toast.makeText(MainActivity.this, "granted", Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("writeStorage", "denied");
                    //Toast.makeText(MainActivity.this, "denied", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
