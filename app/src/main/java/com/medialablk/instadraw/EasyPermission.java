package com.medialablk.instadraw;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.lang.reflect.Method;

public class EasyPermission {

    private static final int MY_PERMISSIONS_REQUEST = 100;

    private Activity runningActivity;

    public void requestPermission(Activity runningActivity, String permission) {
        this.runningActivity = runningActivity;

        if (ContextCompat.checkSelfPermission(runningActivity,
                permission)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(runningActivity,
                    new String[]{permission},
                    MY_PERMISSIONS_REQUEST);

        } else {
            callInterface(runningActivity, permission, true);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (runningActivity != null) {
                        callInterface(runningActivity, permissions[0], true);
                    }

                } else {
                    if (runningActivity != null) {
                        callInterface(runningActivity, permissions[0], false);
                    }

                }
            }
        }
    }

    private void callInterface(Activity activity, String permission, boolean isGranted) throws InterfaceNotImplementedException {
        Method method;
        try {
            method = activity.getClass().getMethod("onPermissionResult", String.class, boolean.class);
        } catch (NoSuchMethodException e) {
            throw new InterfaceNotImplementedException("please implement EasyPermission.OnPermissionResult interface in your activity to get the permissions result");
        }
        if (method != null) {
            try {
                method.invoke(activity, permission, isGranted);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public interface OnPermissionResult {

        void onPermissionResult(String permission, boolean isGranted);
    }

    private class InterfaceNotImplementedException extends RuntimeException {
        private InterfaceNotImplementedException(String message) {
            super(message);
        }
    }
}